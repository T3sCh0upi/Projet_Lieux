# Projet Lieu
Ici, vous trouverez les premieres version du cahier des charges de notre lieu rêvé. J'ai essayé de reprendre tout les éléments du pad [Lieux KD](http://pad.nkl4.me/p/lieuxKD)


Pour voir les première version du texte en pdf c'est [ici](principal.pdf)

Pour me soumettre vos commentaires, remarques ajout, c'est [ici](https://framagit.org/l3ibi/Projet_Lieux/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)(Par contre il faut un compte je suis désolé... du coup, vous pouvez me les transmettre en direct sur IRS ou framateam).

Sinon, j'ai mis en place un pad, pour que ce soit plus simple pour vous d'ajouter des commentaires, corriger les faute d'orthographes, et ajouter du contenu. [ici](http://pad.nkl4.me/p/Projet_Lieu_LaTeX)

Je sais que la syntaxe de LaTeX n'est pas toujours évidente, malheureusement c'est celle avec laquelle je suis plus à l'aise. J'ai fait une bascule en markdown [ici](sortie.md)

