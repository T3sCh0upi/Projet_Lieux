*Ce document vise à établir le cahier des charges du prochain lieu de
KD.*

Objectif du lieu
=================

L’objectif de ce projet de lieu est de réunir dans un même lieu
plusieurs associations ayant des valeurs compatibles. L’objectif est à
la fois d’offir plus de visibilité aux différentes initiatives
citoyennes mais aussi de créer les conditions de synergie.

C’est l’occasion aussi de voir les choses en grand, de créer à
Saint-Brieuc un tiers lieu. Un tiers lieu, pour se former à de nouvelles
pratiques, élargir nos manières de voir, toucher de nouveaux publics (qui
jusqu’ici est noyé dans la disparité des initiatives). Ce serait aussi
l’occasion de créer des agendas, des expériences, des événements communs
à l’intérieur d’un lieu qui nous en offrirait la possibilité. C’est
enfin l’occasion de créer un “*espace sécurisé*”[1].

Situation du prochain lieu
===========================

Ce lieu devrait, dans l’idéal se situer au centre de la ville, ou au
moins rapidement accessible depuis le centre. Cette situation
géographique permettrait aussi de faire entrer des curieux·ses.

La situation géographique centrale, est aussi liée à la notion
d’*espace sécurisé*. Il correspond à un besoin de sécurité dans nos
déplacements quotidiens, il évite ainsi aux personnes seules, lorsqu’elles
rentrent tard, et surtout dans des lieux pas ou peu éclairés, de se
sentir en sécurité. La vie, les animations, la lueur des réverbères
participent à ce sentiment de confiance à l’intérieur de l’espace public.
Nous savons par exemple que l’espace public à la tombée de la nuit est
majoritairement masculin, cet écart est généralement atténué à
l’interieur des centres villes.

Accesibilité
============

L’accessibilité est ici en lien avec la situation géographique, mais
aussi avec les infracstructures permettant aux personnes à mobilité
réduite[2] d’accéder à **tous** nos espaces. Il n’est pas rare en effet
de voir certains lieux n’ayant qu’une partie de leur espace accessible,
bien que ces efforts soient louables et participent d’une meilleure
inclusion de ce public cet état de fait reste toujours un frein.

Associations imaginées comme partenaire
=======================================

Ce que nous imaginons
=====================

Ilôt central et restauration
----------------------------

Nous imaginons un vaste lieu avec un ilôt central, avec à la fois un
espace pour faire des présentations, mais aussi un lieu de rencontres et
de restauration rapide, avec pourquoi pas des plats composés avec des
produits locaux, l’occasion de promouvoir la richesse de notre
territoire.

Crèche
------

Nous imaginons aussi dans ce lieu un espace crèche ou garderie,
facilitant ainsi l’accès aux parents seuls. Ceux-ci pourraient alors
rejoindre l’espace de coworking, avec accès internet pendant que leurs
enfants seraient confiés entre de bonnes mains.

Fablab
------

Nous pourrions retrouver à l’intérieur de cet espace, un lieu type
fablab qui permettrait de bricoler, de faire grandir des projets et de
fabriquer à peu de frais des prototypes. Cet espace abriterait des
machines : imprimante 3D, découpeuse laser, et autre outillages.

Bureaux
-------

Des bureaux permettraient d’accueillir des travailleurs indépendants,
désirant résider à l’intérieur de cet espace, et leur permettant ainsi
de disposer librement de l’infrastructure et des services de ce lieu.
Ces lieux devraient être peu sonores afin de ne pas être géner par les
autres activités de ce lieu.

Salle de réunion
----------------

Il est sera aussi intéressant d’avoir des salles de réunion
permettant aux associations hébergées mais aussi aux travailleurs
indépendants de pouvoir recevoir du public et accueillir des réunions, ce
sera alors l’occasion de faire découvrir ce lieu à des personnes de
passage.

Lieu convivial
--------------

Le lieu de vie est primordial pour la synergie du lieu, il est
important que les personnes qui résident, travaillent, ou juste de passage
dans ce lieu puissent se croiser, échanger autour d’un café. Ces
rencontres permettront sans nul doute de faire émerger des idées,
débloquer un problème (lorsque l’on expose son problème à une autre
personne il est déjà résolu à 50%).

Locaux potentiels
=================

[1] Sécurisé dans le temps et l’espace. Créer la possibilité
d’expérimenter de nouvelles alternatives, sécurisé dans le sens où
l’échec n’est pas stigmatisant mais faisant partie de l’expérience

[2] PMR
